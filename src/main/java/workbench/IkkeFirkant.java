package workbench;

import java.util.function.Predicate;

public class IkkeFirkant implements Predicate<String> {

    @Override
    public boolean test(String t) {
        return !t.contains("firkant");
    }
}
