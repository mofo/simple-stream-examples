package workbench;

import java.util.function.Function;

public class  FargBlå implements Function<String, String> {

    @Override
    public String apply(String t) {
        //Split string on empty space.
        String[] s = t.split("\\s");

        //Return "blå" + geometric figure
        return "blå " + s[1];
    }
}
