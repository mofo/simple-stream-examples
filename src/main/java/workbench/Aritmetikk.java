package workbench;

import java.util.stream.Stream;

public class Aritmetikk {

    public Aritmetikk() {

        // Oppretter en strøm av tall fra 1 til 10. 
        Stream<Integer> enTilTi = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        // Vi utfører operasjoner på strømmen av tall. Live demo.
        // 1. SKRIV UT alle tallene. (foreach)
        // 2. FILTRER vekk partall. (filter)
        // 3. Kjør PARALLELLT. Obs! Obs! Tenk på om rekkefølge er viktig! (parallel)
    }

    // Kjør eksempelet
    public static void main(String[] args) {
        new Aritmetikk();
    }
}