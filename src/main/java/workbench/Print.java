package workbench;

import java.util.function.Consumer;

public class Print implements Consumer<Object>{

    @Override
    public void accept(Object t) {
        System.out.println(t.toString());
    }
}
