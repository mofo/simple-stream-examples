package workbench;

import java.util.stream.Stream;

public class FargerikGeometri {

    public FargerikGeometri() {

        // Oppretter en strøm av tekststrenger. 
        // Hver tekststreng er en farge og en geometrisk figur.
        Stream<String> fargedeFigurer = Stream.of(
                "blå firkant",
                "rød sirkel",
                "gul trekant",
                "rød firkant",
                "gul firkant",
                "blå trekant",
                "gul sirkel",
                "rosa trekant");

        // Vi utfører operasjoner på strømmen av fargede geometriske figurer. Live demo
        // 1. SKRIV UT alle figurene. (foreach)
        // 2. FILTRER vekk alt som ikke er firkanter (filter)
        // 3. FARG alle firkantene blå (map)
    }

    // Kjør eksempelet
    public static void main(String[] args) {
        new FargerikGeometri();
    }
}
