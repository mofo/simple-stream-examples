# Simple stream tutorial

This tutorial was created to teach simple concepts of streams. Suggested reading about streams http://winterbe.com/posts/2014/07/31/java8-stream-tutorial-examples/

According to Winterbe
"Streams are Monads, thus playing a big part in bringing functional programming to Java:

>In functional programming, a monad is a structure that represents computations defined as sequences of steps. A type with a monad structure defines what it means to chain operations, or nest functions of that type together."